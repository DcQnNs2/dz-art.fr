<?php
define('SEONID', strstr($_SERVER['HTTP_USER_AGENT'], 'seonid') ? true : false);
ini_set('display_errors', SEONID);

if(SEONID) {//debug mode
    http_response_code(404);
    $start_time = microtime(true);
} else
    http_response_code(503);

define('INDEXER', true);
define('MASTER_HOST', 'http://46.101.196.51/');
define('SCHEME', empty($_SERVER['HTTPS']) ? 'http://' : 'https://');
$path = substr(preg_replace('#\?.+#', '', $_SERVER['REQUEST_URI']), 1);
if(preg_match('#/\d+$#', $path, $m)) {
    $path = preg_replace('#/\d+$#', '', $path);
    $pagination = substr($m[0], 1)+0;
} else
    $pagination = 1;
unset($m);
define('PATH', $path);
unset($path);
define('HOST', $_SERVER['HTTP_HOST']);

$photoho_hosts = array('topcadouripersonalizate.com','robroker.com','albume-foto-de-nunta.robroker.com','album-foto-a4.robroker.com','albume-nunti.robroker.com','albume-foto-absolventi.robroker.com','albume-foto-personalizate.robroker.com','albumul-nuntii-noastre.robroker.com','topalbumefoto.com','topcalendarepersonalizate.com','topfotografnunta.com','printfotografii.com','albumfotocadou.com','topfotocarte.com','albumul-nuntii-noastre.treistele.ro','robroker.ro','nid.ro','carti-foto.nid.ro','album-digital.nid.ro','foto.nid.ro','album-fotografii.nid.ro','foto-carte.nid.ro','foto-carti.nid.ro','albume-foto.nid.ro','fotocarti.nid.ro','carte-foto.nid.ro','fotocarte.nid.ro','foto-albume.nid.ro','editare-foto.nid.ro','albume-digitale.nid.ro','album-nunta-pret.nid.ro','albume-nunta-preturi.nid.ro','top.nid.ro','owner.ro','album-foto-copii.owner.ro','fotocarte-voucher.owner.ro','fotocarte-oferta.owner.ro','albume-foto-digitale-preturi.owner.ro','albume-foto-ieftine.owner.ro','7stele.ro','fotocarti.7stele.ro','album-digital.7stele.ro','carte-foto.7stele.ro','foto.7stele.ro','albume-fotografii.7stele.ro','album-foto.7stele.ro','foto-albume.7stele.ro','foto-carte.7stele.ro','photo.7stele.ro','foto-carti.7stele.ro','album-personalizat.7stele.ro','album-fotografii.7stele.ro','editare-foto.7stele.ro','carti-foto.7stele.ro','album-fotocarte-pret.7stele.ro','album-de-nunta-pret.7stele.ro','pret-album-foto-nunta.7stele.ro','preturi-albume-foto.7stele.ro','album-foto-digital.7stele.ro','albume-foto-nunta-personalizate.7stele.ro','albume-foto-bebelusi.7stele.ro','albume-botez.7stele.ro','albume-foto-piele.7stele.ro','fotocarte-online.7stele.ro','album-poze.7stele.ro','andreimuresanu.ro','album-foto-tip-carte.andreimuresanu.ro','album-digital-nunta.andreimuresanu.ro','albume-foto-personalizate.andreimuresanu.ro','album-foto-profesional.andreimuresanu.ro','carti-fotografie.andreimuresanu.ro','albume-foto-digitale-preturi.andreimuresanu.ro','albume-foto-vanzare.andreimuresanu.ro','carti-fotografie-digitala.andreimuresanu.ro','album-nunta.andreimuresanu.ro','bestcluj.ro','album-foto-digital-preturi.bestcluj.ro','albume-foto-cartonate.bestcluj.ro','album-foto-pret.bestcluj.ro','album-foto-profesional.bestcluj.ro','album-nunta.bestcluj.ro','album-personalizat.bestcluj.ro','pret-album-foto.bestcluj.ro','album-foto-10x15.bestcluj.ro','cartierindustrial.ro','albume-foto.cartierindustrial.ro','album-foto-tip-carte.cartierindustrial.ro','fotocarte.cartierindustrial.ro','carte-fotografie.cartierindustrial.ro','album-foto-personalizat.cartierindustrial.ro','album-foto-copii.cartierindustrial.ro','carti-fotografie-digitala.cartierindustrial.ro','foto-carti.cartierindustrial.ro','dambulrotund.ro','albume-foto-nunta-pret.dambulrotund.ro','preturi-albume-foto.dambulrotund.ro','albume-foto-botez.dambulrotund.ro','pret-album-foto-nunta.dambulrotund.ro','albume-absolvire.dambulrotund.ro','albume-foto-preturi.dambulrotund.ro','fotocarte.dambulrotund.ro','albume-foto.dambulrotund.ro','carte-foto.dambulrotund.ro','album-foto-personalizat.dambulrotund.ro','fatima.ro','modele-de-albume-foto.fatima.ro','album-poze-nunta.fatima.ro','coperti-albume-foto.fatima.ro','gilau.ro','albume-foto-de-nunta.gilau.ro','album-pentru-fotografii.gilau.ro','album-fotografii.gilau.ro','album-foto-de-nunta.gilau.ro','albume-foto-nunti.gilau.ro','albume-foto-personalizate-pret.gilau.ro','foto-albume.gilau.ro','album-nunta.gilau.ro','interbancar.ro','album-personalizat.interbancar.ro','foto-albume.interbancar.ro','fotocarte.interbancar.ro','kali.ro','foto-carte.kali.ro','album-foto-romania.kali.ro','albumul-nuntii-noastre.kali.ro','album-foto-ieftin.kali.ro','album-foto-bebe.kali.ro','albume-foto-nunta-personalizate.kali.ro','marasti.ro','fotocarte-nunta.marasti.ro','carti-foto.marasti.ro','album-de-nunta-pret.marasti.ro','mirific.ro','fotocarti.mirific.ro','album-fotografii.mirific.ro','foto.mirific.ro','foto-carte.mirific.ro','foto-albume.mirific.ro','photo-album.mirific.ro','photo.mirific.ro','foto-album.mirific.ro','carte-foto.mirific.ro','foto-carti.mirific.ro','album-digital.mirific.ro','carti-foto.mirific.ro','albume-foto-clasice.mirific.ro','albume-foto-nunta-personalizate.mirific.ro','albume-fotografii.mirific.ro','albume-foto-online.mirific.ro','albume-nunta.mirific.ro','album-poze.mirific.ro','muresanu.ro','albume-nunta.muresanu.ro','carti-fotografie.muresanu.ro','albume-foto-scolare.muresanu.ro','albume-foto-de-nunta.muresanu.ro','niceone.ro','albume-fotografii.niceone.ro','fotocarte.niceone.ro','album-fotografii.niceone.ro','album-foto.niceone.ro','foto-album.niceone.ro','photo.niceone.ro','foto-carti.niceone.ro','foto.niceone.ro','albume-de-poze.niceone.ro','album-foto-10x15.niceone.ro','albume-foto-de-vanzare.niceone.ro','fotocarte-pret.niceone.ro','pret-album-foto.niceone.ro','album-poze-nunta.niceone.ro','albume-nunta.niceone.ro','album-nunta-pret.niceone.ro','pusculita.ro','foto-album.pusculita.ro','photo-album.pusculita.ro','album-fotografii.pusculita.ro','photo.pusculita.ro','foto-carti.pusculita.ro','editare-foto.pusculita.ro','fotocarte.pusculita.ro','foto-albume.pusculita.ro','albume-fotografii.pusculita.ro','fotocarti.pusculita.ro','album-personalizat.pusculita.ro','albume-foto-pret.pusculita.ro','album-foto-ieftin.pusculita.ro','album-foto-copii.pusculita.ro','album-foto-nunta.pusculita.ro','coperti-albume-foto.pusculita.ro','saptestele.ro','fotocarte.saptestele.ro','album-digital.saptestele.ro','albume-fotografii.saptestele.ro','photo-album.saptestele.ro','editare-foto.saptestele.ro','foto-carte.saptestele.ro','albume-foto.saptestele.ro','foto.saptestele.ro','foto-album.saptestele.ro','foto-carti.saptestele.ro','fotocarti.saptestele.ro','albume-foto-carte.saptestele.ro','modele-albume-foto-nunta.saptestele.ro','album-foto-personalizat.saptestele.ro','albume-nunta.saptestele.ro','fotocarte-oferta.saptestele.ro','album-foto-de-nunta.saptestele.ro','album-foto-a4.saptestele.ro','albume-foto-deosebite.saptestele.ro','sfetnic.ro','carti-foto.sfetnic.ro','fotocarte.sfetnic.ro','fotocarti.sfetnic.ro','carte-foto.sfetnic.ro','foto-carti.sfetnic.ro','albume-fotografii.sfetnic.ro','foto.sfetnic.ro','album-foto.sfetnic.ro','foto-carte.sfetnic.ro','albume-foto.sfetnic.ro','foto-albume.sfetnic.ro','album-personalizat.sfetnic.ro','editare-foto.sfetnic.ro','album-fotografii.sfetnic.ro','album-digital.sfetnic.ro','albume-foto-botez.sfetnic.ro','albume-foto-personalizate-online.sfetnic.ro','fotocarte-nunta.sfetnic.ro','coperta-album-foto.sfetnic.ro','albume-foto-vanzare.sfetnic.ro','tararomaneasca.ro','fotocarte-oferta.tararomaneasca.ro','albume-foto-personalizate.tararomaneasca.ro','albume-foto-absolvire.tararomaneasca.ro','albume-absolventi.tararomaneasca.ro','albume-personalizate.tararomaneasca.ro','album-foto-10x15.tararomaneasca.ro','carte-fotografie.tararomaneasca.ro','album-pentru-fotografii.tararomaneasca.ro','albume-foto-nunta-pret.tararomaneasca.ro','album-nunta-pret.tararomaneasca.ro','albume-foto-nunta.tararomaneasca.ro','wallst.ro','fotocarti.wallst.ro','album-foto.wallst.ro','album-fotografii.wallst.ro','album-digital.wallst.ro','album-personalizat.wallst.ro','photo-album.wallst.ro','carti-foto.wallst.ro','foto-album.wallst.ro','foto-carti.wallst.ro','photo.wallst.ro','albume-foto.wallst.ro','foto-albume.wallst.ro','album-foto-online.wallst.ro','albume-foto-vanzare.wallst.ro','albume-foto-preturi.wallst.ro','albumul-nuntii-noastre.wallst.ro','fotocarte-voucher.wallst.ro','albume-foto-pret.wallst.ro','zorilor.ro','albume-foto-botez.zorilor.ro','fotocarte-online.zorilor.ro','album-foto-13x18.zorilor.ro','album-foto-romania.zorilor.ro','albume-foto-de-vanzare.zorilor.ro','albume-absolvire.zorilor.ro','fotocarte-pret.zorilor.ro','coom.ro','foto-carte.coom.ro','foto-album.coom.ro','album-fotografii.coom.ro','albume-fotografii.coom.ro','fotocarti.coom.ro','album-foto.coom.ro','fotocarte.coom.ro','editare-foto.coom.ro','foto-carti.coom.ro','foto.coom.ro','foto-albume.coom.ro','photo.coom.ro','albume-foto-scolare.coom.ro','feleacu.ro','albume-foto-deosebite.feleacu.ro','album-de-nunta.feleacu.ro','album-foto-bebe.feleacu.ro','albume-nunti.feleacu.ro','albume-foto-cartonate.feleacu.ro','albume-foto-nunta-pret.feleacu.ro','carti-foto.robroker.ro','program-de-facut-albume-foto.robroker.ro','treistele.ro','foto-album.treistele.ro','foto.treistele.ro','albume-fotografii.treistele.ro','carte-foto.treistele.ro','foto-carti.treistele.ro','album-personalizat.treistele.ro','photo-album.treistele.ro','album-foto.treistele.ro','foto-albume.treistele.ro','album-fotografii.treistele.ro','photo.treistele.ro','albume-foto-bebelusi.treistele.ro','albume-foto-de-nunta.treistele.ro','albume-foto-deosebite.treistele.ro','album-poze-nunta.treistele.ro','foto-carte.treistele.ro','album-foto-a4.treistele.ro','album-foto-13x18.treistele.ro','netid.ro','fotocarte.netid.ro','albume-foto-preturi.netid.ro','album-foto.netid.ro','album-nunta-pret.netid.ro','album-foto-profesional.netid.ro','zina.ro','editare-foto.zina.ro','carte-foto.zina.ro','carti-foto.zina.ro','photo.zina.ro','album-personalizat.zina.ro','album-digital.zina.ro','foto-albume.zina.ro','photo-album.zina.ro','foto-album.zina.ro','albume-fotografii.zina.ro','album-foto.zina.ro','albume-foto.zina.ro','fotocarte.zina.ro','album-fotografii.zina.ro','albume-de-poze.zina.ro','album-foto-digital.zina.ro','album-foto-de-nunta.zina.ro','oorg.ro','foto.oorg.ro','editare-foto.oorg.ro','album-personalizat.oorg.ro','foto-albume.oorg.ro','photo.oorg.ro','albume-fotografii.oorg.ro','foto-carte.oorg.ro','fotocarte.oorg.ro','foto-album.oorg.ro','photo-album.oorg.ro','carte-foto.oorg.ro','albume-foto-de-vanzare.oorg.ro','albume-botez.oorg.ro','7stele.com','album-digital.7stele.com','album-personalizat.7stele.com','albume-fotografii.7stele.com','album-foto.7stele.com','albume-foto.7stele.com','album-fotografii.7stele.com','carte-foto.7stele.com','photo.7stele.com','editare-foto.7stele.com','foto-albume.7stele.com','photo-album.7stele.com','album-poze.7stele.com','albume-foto-online.7stele.com','foto-carte.7stele.com','album-nunta-pret.7stele.com','album-foto-cadou.7stele.com','album-foto-lemn.7stele.com','3stele.com','album-digital.3stele.com','carti-foto.3stele.com','album-foto.3stele.com','album-fotografii.3stele.com','photo-album.3stele.com','albume-fotografii.3stele.com','editare-foto.3stele.com','album-personalizat.3stele.com','albume-foto.3stele.com','foto-carte.3stele.com','foto-album.3stele.com','carti-fotografie.3stele.com','albume-botez.3stele.com','album-foto-digital.3stele.com','album-foto-personalizat.3stele.com','coperta-album-foto.3stele.com','album-foto-online.3stele.com','saptestele.com','album-digital.saptestele.com','fotocarti.saptestele.com','editare-foto.saptestele.com','albume-fotografii.saptestele.com','carti-foto.saptestele.com','photo-album.saptestele.com','album-personalizat.saptestele.com','albume-foto.saptestele.com','photo.saptestele.com','foto-albume.saptestele.com','foto-carte.saptestele.com','fotocarte.saptestele.com','carte-fotografie.saptestele.com','albume-personalizate.saptestele.com','album-foto-digital-preturi.saptestele.com','albume-foto-absolventi.saptestele.com','albume-foto-nunti.saptestele.com','albume-foto-deosebite.saptestele.com','pusculita.com','foto-carti.pusculita.com','photo.pusculita.com','foto.pusculita.com','photo-album.pusculita.com','carte-foto.pusculita.com','foto-albume.pusculita.com','album-digital.pusculita.com','foto-carte.pusculita.com','album-fotografii.pusculita.com','albume-fotografii.pusculita.com','carti-foto.pusculita.com','album-personalizat.pusculita.com','albume-foto-absolventi.pusculita.com','albume-foto-carte.pusculita.com','albume-foto-nunta-personalizate.pusculita.com','coperti-albume-foto.pusculita.com','carti-fotografie-digitala.pusculita.com','album-botez.pusculita.com','album-foto-10x15.pusculita.com','treistele.com','foto-carti.treistele.com','photo-album.treistele.com','photo.treistele.com','albume-foto.treistele.com','albume-fotografii.treistele.com','album-digital.treistele.com','carti-foto.treistele.com','album-fotografii.treistele.com','foto-album.treistele.com','carte-foto.treistele.com','album-foto.treistele.com','foto-albume.treistele.com','foto-carte.treistele.com','albume-foto-botez.treistele.com','albume-foto-preturi.treistele.com','albume-de-nunta.treistele.com','potaissa.com','album-personalizat.potaissa.com','album-foto-personalizat.potaissa.com','fotocarte-oferta.potaissa.com','modele-albume-foto-nunta.potaissa.com','fotocarte-voucher.potaissa.com');

if(PATH AND $pos = strrpos(PATH, '.') AND $ext = strtolower(substr(PATH, $pos+1)) AND strlen($ext) <= 4 AND !in_array($ext, array('html', 'php'))) {//assets
    if(in_array(HOST, $photoho_hosts)) {//temp photogo
        if(file_exists('websites/'.HOST.'/'.PATH) AND $c = file_get_contents('websites/'.HOST.'/'.PATH)) {
        } elseif($c = @file_get_contents(MASTER_HOST.'websites/'.HOST.'/'.PATH)) {//temp photogo
            $dir = '';
            $dirs = explode('/', 'websites/'.HOST.'/'.PATH);
            array_pop($dirs);
            foreach($dirs as $_dir) {
                $dir .= ($dir ? '/' : '').$_dir;
                if(!is_dir($dir)) mkdir($dir);
            }
            file_put_contents('websites/'.HOST.'/'.PATH, $c);
        }
        if($c) {
            include 'mimes.php';
            if(isset($mimes[$ext]))
                $mime = $mimes[$ext];
            else
                $mime = 'text/plain';
            http_response_code(200);
            header('Content-Type: '.$mime);
            exit($c);
        }
    } elseif(substr(PATH, 0, 7) == 'assets/' AND $c = @file_get_contents(MASTER_HOST.PATH)) {//assets
        $dir = '';
        $dirs = explode('/', PATH);
        array_pop($dirs);
        foreach($dirs as $_dir) {
            $dir .= ($dir ? '/' : '').$_dir;
            if(!is_dir($dir)) mkdir($dir);
        }
        file_put_contents(PATH, $c);
        include 'mimes.php';
        if(isset($mimes[$ext]))
            $mime = $mimes[$ext];
        else
            $mime = 'text/plain';
        http_response_code(200);
        header('Content-Type: '.$mime);
        exit($c);
    } elseif(($ext == 'jpg' OR $ext == 'png') AND $path = preg_replace('#^images/#', '', PATH) AND ($c = @file_get_contents(MASTER_HOST.'generated2/'.$path) OR $c = @file_get_contents(MASTER_HOST.'generated/'.$path))) {
        //todo: use static.domain_name which points to blinkping ip...
        $dir = '';
        $dirs = explode('/', PATH);
        array_pop($dirs);
        foreach($dirs as $_dir) {
            $dir .= ($dir ? '/' : '').$_dir;
            if(!is_dir($dir)) mkdir($dir);
        }
        file_put_contents(PATH, $c);
        if($ext == 'png')
            header('Content-Type: image/png');
        else
            header('Content-Type: image/jpeg');
        http_response_code(200);
        exit($c);
    }
    not_found();
}
if(SEONID) {
    register_shutdown_function(function() {
        global $start_time;
        echo 'Loaded in: '.round(microtime(true)-$start_time, 2);
    });
}

include 'db.php';

if(!db_connect()) {
    if(PATH)
        redirect(SCHEME.HOST);
    default_page();
    exit;
}

set_time_limit(600);

session_start();
if($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(empty($_REQUEST['phone']) OR empty($_REQUEST['token']) OR empty($_SESSION['token']) OR $_REQUEST['token'] != $_SESSION['token']) {
        http_response_code(403);
        exit;
    }
    http_response_code(200);
    unset($_SESSION['token']);
    unset($_REQUEST['token']);
    db_query('INSERT INTO visitor_messages ?e', $_REQUEST);
    exit;
}
if(empty($_SESSION['token']))
    $_SESSION['token'] = md5(uniqid(time(), true));


if($r = db_get_row('SELECT url, redirect_type FROM domain_redirects WHERE host=?s', HOST)) {
    $url = rtrim($r['url'], '/').'/';
    if($r['redirect_type'] == 'O')
        $url .= PATH;
    redirect($url, 301);
}

$subdomain = db_get_row('SELECT * FROM subdomains WHERE subdomain=?s', HOST);
if(!$subdomain)
    default_page();
if($subdomain['is_banned'] AND PATH)
    redirect(SCHEME.HOST, 301);

if(!$subdomain['network_id']) {//sandbox
    $content = db_get_field('SELECT content FROM sandbox_pages WHERE subdomain_id=?i AND seo_name=?s LIMIT 1', $subdomain['subdomain_id'], PATH);
    if($content) {
        fixit($content);
        if(!SEONID) http_response_code(200);
        to_html($content);
    } elseif(!PATH)
        default_page();
    exit;
}

if(!$subdomain['template_id'] OR $subdomain['network_id'] == '5') {//temp 5
    default_page();
}

if(PATH == 'terms') {
    $page = array();
    $page['branch'] = 'T';
    $page['parent_id'] = 0;
    $page['title'] = 'Conditions générales d\'utilisation du site';
    $page['terms'] = @file_get_contents(MASTER_HOST.'assets/5/terms.txt');
    $page['legal'] = '[site_brand]® est une marque déposée et exploitée par la société [site_brand] SAS <br>Contact : [tel]';
    $page_data = array();
} else {
    $page = db_get_row('SELECT * FROM network_pages WHERE subdomain_id=?i AND seo_name=?s LIMIT 1', $subdomain['subdomain_id'], str_replace('.html', '', PATH));
    if(!$page)
        default_page();
}

$site_data = json_decode($subdomain['data'], true);
if(!$site_data) $site_data = array();
$page = array_merge($site_data, $page);

if(PATH != 'terms') {
$page_content = get_page_content($page['page_id']);
if(!$page_content) {
    if(PATH) redirect(SCHEME.HOST, 301);
}
$page_data = $page_content['page_data'] ? $page_content['page_data'] : array();
if($page_content['page_content'])
    $page_data = array_merge($page_data, $page_content['page_content']);
unset($page_content);
}

$page = array_merge($page, $page_data, db_get_row('SELECT lang FROM networks WHERE network_id=?i', $subdomain['network_id']));
unset($page_data);

if(PATH != 'terms') {
if(isset($page['merge_with_parent']) AND $page['parent_id']) {
    $page = array_merge(get_page_content($page['parent_id'], 'page_data'), $page);
    unset($page['merge_with_parent']);
}

if($page['parent_id']) {
    $page['breadcrumb'] = array();
    $page['breadcrumb'][] = array('anchor' => $page['anchor'], 'href' => '/'.PATH, 'last' => 1);
    if(PATH) {
        $parent = db_get_row('SELECT parent_id, seo_name, anchor FROM network_pages WHERE page_id=?i', $page['parent_id']);
        while($parent) {
            $page['breadcrumb'][] = array('anchor' => ($parent['anchor'] ? $parent['anchor'] : ($parent['seo_name'] ? $parent['seo_name'] : ($page['site_brand'] ? $page['site_brand'] : HOST))), 'href' => '/'.$parent['seo_name']);
            $_parent_id = $parent['parent_id'];
            $parent = db_get_row('SELECT parent_id, seo_name, anchor FROM network_pages WHERE page_id=?i', $parent['parent_id']);
            if($parent['parent_id'] == $_parent_id) break;//dani bug
        }
    }
    $page['breadcrumb'] = array_reverse($page['breadcrumb']);
}

$text_links = $list_links = $table_links = array();
$links = '';
$links = $subdomain['is_banned'] ? '' : db_get_field('SELECT links FROM network_pages_add WHERE page_id=?i LIMIT 1', $page['page_id']);
if($links) {
    $links = json_decode($links, true);
    foreach($links as $v) {
        $to_page = db_get_row('SELECT a.seo_name, b.subdomain, b.network_id, b.has_ssl FROM network_pages a JOIN subdomains b ON b.subdomain_id=a.subdomain_id AND b.is_banned=0 WHERE a.page_id=?i', $v['to_id']);
        $link_params = $v['link_params'] ? $v['link_params'] : array();
        $link = array(
            'href' => ($to_page['has_ssl'] == 'Y' ? 'https' : 'http').'://'.$to_page['subdomain'].'/'.$to_page['seo_name'],
            'anchor' => $v['anchor'],
            'anchor_wrap' => array_filter(explode(',', $v['anchor_wrap'])),
            'link_params' => $link_params,
            'link_type' => $v['link_type'],
        );
        if($v['link_type'] == 'L') {
            $list_links[] = $link;
        } elseif($v['link_type'] == 'A') {
            $table_links[] = $link;
        } elseif(!empty($link_params['placeholder']))
            $text_links[$link_params['placeholder']][] = $link;
        else
            $text_links['T'][] = $link;
    }
}
unset($links);
}
$placeholders = holderize($page);

$template = db_get_field('SELECT template FROM templates WHERE template_id=?i', $subdomain['template_id']);
//$template = file_get_contents('file.html');//temp

$articles_per_page = 9;$has_more_articles = 0;$total_pages = 0;
$template = preg_replace_callback('#(\{\*.+?\*\})|\{((?:\w|\$|/)[^}]+)\}#s', function($_m) use($page) {
    static $in_function;
    if($_m[1]) return '';//comments
    if(preg_match('#^(\$?\w+)\s+(.+)$#', $_m[2], $m)) {
        if($m[1] == 'elseif') $m[1] = '} '.$m[1];
        switch($m[1]) {
            case 'use':
                $params = explode(' ', preg_replace('#\s+#', ' ', $m[2]));
                return '<?php '.array_shift($params).'('.replace_template_var(implode(', ', $params), $in_function).') ?>';
                break;
            case 'block':
                $params = explode(' ', preg_replace('#\s+#', ' ', $m[2]));
                $in_function = true;
                return '<?php function '.array_shift($params).'('.implode(', ', $params).') { global $page, $has_more_articles, $pagination, $total_pages;  ?>';
                break;
            case 'foreach':
                $m[2] = explode(' ', preg_replace('#\s+#', ' ', $m[2]));
                $m[2][0] = replace_template_var($m[2][0], $in_function);
                $m[1] = 'if(!empty('.$m[2][0].')) '.$m[1];
                $m[2] = $m[2][0].' as $'.$m[2][1].(isset($m[2][2]) ? ' => $'.$m[2][2] : '');
                break;
            case '} elseif':
            case 'if':
            case 'while':
                $m[2] = replace_template_var($m[2], $in_function);
                break;
            default:
                if(substr($m[1], 0, 1) == '$')
                    return '<?php '.parse_var($m[0], false, $in_function).' ?>';
                //debug('Regex: '.$m[1].' not implemented');
                //debug($m);
                return '{'.$_m[2].'}';
        }
        return '<?php '.$m[1].'('.$m[2].') { ?>';
    } else {
        if($_m[2] == 'else') return '<?php } else { ?>';
        $first_char = substr($_m[2], 0, 1);
        if($first_char == '/') {
            $ob = substr($_m[2], 1);
            if(!in_array($ob, array('foreach', 'if', 'while', 'block'))) return '{'.$_m[2].'}';
            if($ob == 'block')
                $in_function = false;
            return '<?php } ?>';
        }
        if(strpos($_m[2], '$') !== false OR strpos($_m[2], '(') !== false) {
            return '<?php '.parse_var($_m[2], true, $in_function).' ?>';
        }
        if(defined($_m[2]))
            return '<?php echo '.$_m[2].' ?>';
        return '{'.$_m[2].'}';
    }
}, $template);

//debug($template);exit;//temp
ob_start();
@eval('?>'.$template);
$content = ob_get_clean();

if($content) {
    fixit($content);
    if(!SEONID) http_response_code(200);
    to_html($content);
}
exit;

function to_html($content) {
    $path = PATH ? PATH : 'index.html';
    $dir = '';
    $dirs = explode('/', $path);
    array_pop($dirs);
    foreach($dirs as $_dir) {
        $dir .= ($dir ? '/' : '').$_dir;
        if(!is_dir($dir)) mkdir($dir);
    }
    file_put_contents($path, $content);
    echo $content;
    exit;
}
function default_page() {
    global $photoho_hosts;
    if(0 AND in_array(HOST, $photoho_hosts)) {//temp photogo, temp 0
        $path = PATH ? PATH : 'index.html';
        if(file_exists('websites/'.HOST.'/'.$path) AND $c = file_get_contents('websites/'.HOST.'/'.$path)) {
        } elseif($c = @file_get_contents(MASTER_HOST.'websites/'.HOST.'/'.$path)) {
            $dir = '';
            $dirs = explode('/', 'websites/'.HOST.'/'.$path);
            array_pop($dirs);
            foreach($dirs as $_dir) {
                $dir .= ($dir ? '/' : '').$_dir;
                if(!is_dir($dir)) mkdir($dir);
            }
            file_put_contents('websites/'.HOST.'/'.$path, $c);
        }
        if($c) {
            if(!SEONID) http_response_code(200);
            header('Content-Type: text/html');
            exit($c);
        }
    }
    if(PATH)
        redirect(SCHEME.HOST, 301);
    if(!SEONID) http_response_code(200);
    exit(@file_get_contents('index.html_default'));
}

//function get_county_name($county_id) {
//    return db_get_field('SELECT county FROM geo_counties WHERE county_id=?i', $county_id);
//}
function count_level($branch, $level, $g = array()) {
    global $subdomain;
    $condition = db_quote('subdomain_id=?i AND branch=?s AND level=?i', $subdomain['subdomain_id'], $branch, $level);
    if($level > 1) {
        foreach($g as $k => $v) {
            if($v)
                $condition .= db_quote(' AND ?p=?i', 'g'.($k+1), $v);
        }
    }
    $condition .= db_quote(' AND ?p<>0', 'g'.$level);
    //debug($condition);
    return db_get_field('SELECT COUNT(*) FROM network_pages WHERE ?p', $condition);
}
/*function get_branches() {
    global $subdomain;
    return db_get_array('SELECT * FROM network_pages WHERE subdomain_id=?i AND branch<>?s AND level=1 AND g1=0 ORDER BY gravity DESC', $subdomain['subdomain_id'], '');
}*/
function get_index($branch, $level = 0, $g = array()) {
    global $subdomain;
    $condition = db_quote('subdomain_id=?i AND branch=?s AND level=?i', $subdomain['subdomain_id'], $branch, $level);
    if($level > 0 AND $g) {
        foreach($g as $k => $v) {
            if($v)
                $condition .= db_quote(' AND ?p=?i', 'g'.($k+1), $v);
        }
    }
    $condition .= db_quote(' AND ?p=0', 'g'.($level+1));
    return db_get_row('SELECT * FROM network_pages WHERE ?p LIMIT 1', $condition);
}
function get_branch_pages($branch, $level, $g = array(), $limit = 20, $exclude = false, $check_next_level = false) {
    global $subdomain, $pagination, $total_pages;
    //$level = count($g)+1;
    $condition = db_quote('subdomain_id=?i AND branch=?s AND level=?i', $subdomain['subdomain_id'], $branch, $level);
    if($level > 0 AND $g) {
        foreach($g as $k => $v) {
            if($v) {
                if(is_array($v)) {
                    $ag = explode(',', current($v));
                    $v = key($v);
                    if($v) {
                        $condition .= db_quote(' AND (?p=?i', 'g'.($k+1), $v);
                        foreach($ag as $i)
                            $condition .= db_quote(' OR ?p=?i', 'g'.$i, $v);
                        $condition .= ')';
                        //debug($condition);
                        //exit;
                    }
                } else
                    $condition .= db_quote(' AND ?p=?i', 'g'.($k+1), $v);
            }
        }
    }
    $condition .= db_quote(' AND ?p<>0', 'g'.$level);
    if($exclude) {
        $condition .= db_quote(' AND ?p<>?i', 'g'.$level, $exclude);
        //todo: order by ...
    }
    //debug($condition);
    $pages = db_get_array('SELECT SQL_CALC_FOUND_ROWS * FROM network_pages WHERE ?p ORDER BY gravity DESC, modified DESC ?p', $condition, $limit ? 'LIMIT '.(($pagination-1)*$limit).','.$limit : '');
    if($limit) {
        $total = db_get_field('SELECT FOUND_ROWS()');
        $total_pages = ceil($total/$limit);
    }
    if($check_next_level) {
        foreach($pages as &$v) {
            $_condition = str_replace(array(' level='.$level.' ', ' g'.$level.'<>0'), array(' level='.($level+1).' ', ' g'.($level+1).'='.$v['g'.($level+1)].' AND g'.($level+2).'<>0'), $condition);
            $next_level = db_get_row('SELECT SQL_CALC_FOUND_ROWS * FROM network_pages WHERE ?p LIMIT 1', $_condition);
            $total = db_get_field('SELECT FOUND_ROWS()');
            if($total == 1)
                $v = $next_level;
        }
        unset($v);
    }
        foreach($pages as &$v) {
            $v = array_merge($v, get_page_content($v['page_id'], 'page_data'));
        }
        unset($v);
    return $pages;
}
function get_submenu($page_id, $limit = 50, $get_subpages = true) {//temp old
    global $subdomain;
    if($subdomain['is_banned']) return array();
    if($limit < 1) $limit = 50;
    $subpages = db_get_array('SELECT page_id, seo_name, anchor, level FROM network_pages WHERE parent_id=?i ORDER BY gravity DESC, RAND() LIMIT ?i', $page_id, $limit);
    if(!$get_subpages) return $subpages;
    $c = count($subpages);
    if($c < $limit) {
        $limit -= $c;
        $limit = ceil($limit/$c);
        $_subpages = $subpages;
        $subpages = array();
        foreach($_subpages as $v) {
            $subpages[] = $v;
            $subpages = array_merge($subpages, get_submenu($v['page_id'], $limit, false));
        }
    }
    return $subpages;
}
function get_articles($page_id, $key, $page = 1) {//temp old
    global $articles_per_page, $has_more_articles, $subdomain;
    if($subdomain['is_banned']) return array();
    if($page < 1) $page = 1;
    $childs = db_get_array('SELECT SQL_CALC_FOUND_ROWS seo_name, anchor, page_id FROM network_pages WHERE parent_id=?i ORDER BY gravity DESC, page_id LIMIT '.(($page-1)*$articles_per_page).','.$articles_per_page, $page_id);
    $total_articles = db_get_field('SELECT FOUND_ROWS()');
    $has_more_articles = ($total_articles > $page*$articles_per_page);
    $articles = array();
    foreach($childs as $v) {
        $_data = get_page_content($v['page_id'], 'page_content');
        if(isset($_data[0])) {//temp dani fucked
            $article = $_data[array_rand($_data)];
            $article['seo_name'] = $v['seo_name'];
            $article['anchor'] = $v['anchor'];
            $articles[] = $article;
        } else
        if(!empty($_data[$key])) {
            $article = $_data[$key][array_rand($_data[$key])];
            if(isset($article[0]))
                $article = $article[array_rand($article)];
            $article['seo_name'] = $v['seo_name'];
            $article['anchor'] = $v['anchor'];
            $articles[] = $article;
        }
    }
    return $articles;
}

function get_page_content($page_id, $only_field = '') {
    static $ips;
    if(!$page_id) return array();
    $droplet_id = db_get_field('SELECT droplet_id FROM network_pages WHERE page_id=?i', $page_id);
    if(!$droplet_id) return array();
    if(!$ips) $ips = array();
    if(!isset($ips[$droplet_id]))
        $ips[$droplet_id] = db_get_field('SELECT ip_address FROM droplets WHERE droplet_id=?i LIMIT 1', $droplet_id);
    $ip = $ips[$droplet_id];
    if(!$ip) return array();
    db_select_db('network_pages', $ip);
    if($only_field)
        $page_data = db_get_field('SELECT '.$only_field.' FROM `pages` WHERE page_id=?i LIMIT 1', $page_id);
    else
        $page_data = db_get_row('SELECT page_data, page_content FROM `pages` WHERE page_id=?i LIMIT 1', $page_id);
    db_select_db();
    if(!$page_data) return array();
    if($only_field) {
        fixit($page_data);
        return ($page_data ? json_decode($page_data, true) : array());
    }
    $page_data['page_data'] = $page_data['page_data'] ? json_decode($page_data['page_data'], true) : array();
    $page_data['page_content'] = $page_data['page_content'] ? json_decode($page_data['page_content'], true) : array();
    if(!is_array($page_data['page_content'])) $page_data['page_content'] = array();
    fixit($page_data);
    return $page_data;
}

function fixit(&$v) {//todo: fix and remove this
    if(!$v) return;
    if(is_array($v)) {
        foreach($v as &$s)
            fixit($s);
    } else {
        $fuckedup = array('Ã€' => 'À', 'Ã' => 'Á', 'Ã‚' => 'Â', 'Ãƒ' => 'Ã', 'Ã„' => 'Ä', 'Ã…' => 'Å', 'Ã†' => 'Æ', 'Ã‡' => 'Ç', 'Ãˆ' => 'È', 
            'Ã‰' => 'É', 'ÃŠ' => 'Ê', 'Ã‹' => 'Ë', 'ÃŒ' => 'Ì', 'Ã' => 'Í', 'ÃŽ' => 'Î', 'Ã' => 'Ï', 'Ã' => 'Ð', 'Ã‘' => 'Ñ', 'Ã’' => 'Ò', 
            'Ã“' => 'Ó', 'Ã”' => 'Ô', 'Ã•' => 'Õ', 'Ã–' => 'Ö', 'Ã—' => '×', 'Ã˜' => 'Ø', 'Ã™' => 'Ù', 'Ãš' => 'Ú', 'Ã›' => 'Û', 'Ãœ' => 'Ü', 
            'Ã' => 'Ý', 'Ãž' => 'Þ', 'ÃŸ' => 'ß', 'Ã' => 'à', 'Ã¡' => 'á', 'Ã¢' => 'â', 'Ã£' => 'ã', 'Ã¤' => 'ä', 'Ã¥' => 'å', 'Ã¦' => 'æ', 'Ã§' => 'ç', 
            'Ã¨' => 'è', 'Ã©' => 'é', 'Ãª' => 'ê', 'Ã«' => 'ë', 'Ã¬' => 'ì', 'Ã­' => 'í', 'Ã®' => 'î', 'Ã¯' => 'ï', 'Ã°' => 'ð', 'Ã±' => 'ñ', 'Ã²' => 'ò', 
            'Ã³' => 'ó', 'Ã´' => 'ô', 'Ãµ' => 'õ', 'Ã¶' => 'ö', 'Ã·' => '÷', 'Ã¸' => 'ø', 'Ã¹' => 'ù', 'Ãº' => 'ú', 'Ã»' => 'û', 'Ã¼' => 'ü', 'Ã½' => 'ý', 'Ã¾' => 'þ', 'Ã¿' => 'ÿ',
            'â‚¬' => '€ ', 'â€š' => '‚ ', 'Æ’' => 'ƒ ', 'â€ž' => '„ ', 'â€¦' => '… ', 'â€' => '† ', 'â€¡' => '‡ ', 'Ë†' => 'ˆ ', 'â€°' => '‰ ', 'Å' => 'Š ', 
            'â€¹' => '‹ ', 'Å’' => 'Œ ', 'Å½' => 'Ž ', 'â€˜' => '‘ ', 'â€™' => '’ ', 'â€œ' => '“ ', 'â€' => '” ', 'â€¢' => '• ', 'â€“' => '– ', 'â€”' => '— ', 
            'Ëœ' => '˜ ', 'â„¢' => '™ ', 'Å¡' => 'š ', 'â€º' => '› ', 'Å“' => 'œ ', 'Å¾' => 'ž ', 'Å¸' => 'Ÿ ', 'Â' => ' ', 'Â¡' => '¡ ', 'Â¢' => '¢ ', 'Â£' => '£ ', 
            'Â¤' => '¤ ', 'Â¥' => '¥ ', 'Â¦' => '¦ ', 'Â§' => '§ ', 'Â¨' => '¨ ', 'Â©' => '© ', 'Âª' => 'ª ', 'Â«' => '« ', 'Â¬' => '¬ ', 'Â­' => ' ', 'Â®' => '® ', 
            'Â¯' => '¯ ', 'Â°' => '° ', 'Â±' => '± ', 'Â²' => '² ', 'Â³' => '³ ', 'Â´' => '´ ', 'Âµ' => 'µ ', 'Â¶' => '¶ ', 'Â·' => '· ', 'Â¸' => '¸ ', 'Â¹' => '¹ ', 
            'Âº' => 'º ', 'Â»' => '» ', 'Â¼' => '¼ ', 'Â½' => '½ ', 'Â¾' => '¾ ', 'Â¿' => '¿'
        );
        if(preg_match('/'.implode('|', array_keys($fuckedup)).'/', $v))
            $v = strtr($v, $fuckedup);
    }
}

function translit($str) {
    return strtr($str, array(
        'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
        'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
        'Ï'=>'I', 'Ñ'=>'N', 'Ń'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
        'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
        'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
        'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ń'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
        'ú'=>'u', 'û'=>'u', 'ü'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f',
        'ă'=>'a', 'î'=>'i', 'â'=>'a', 'ş'=>'s', 'ș'=>'s', 'ţ'=>'t', 'ț'=>'t', 'Ă'=>'A', 'Î'=>'I', 'Â'=>'A', 'Ş'=>'S', 'Ș'=>'S', 'Ţ'=>'T', 'Ț'=>'T',
    ));
}
function seo_name($str, $delimiter = '-') {
    $str = strtolower(translit($str));
    $str = preg_replace('/[^a-z\p{L}0-9]+/ui', $delimiter, $str);
    $str = preg_replace('#('.$delimiter.'){2,}#', $delimiter, $str);
    return trim($str, '-');
}
//function get_rand($arr) {
//    if(!$arr) return false;
//    return $arr[array_rand($arr)];
//}
function express() {
    static $express;
    if(!$express) $express = db_get_fields('SELECT express FROM express');
    return $express[array_rand($express)];
}
function inject_link($description, $placeholder = 'T', $loop_key = false) {
    global $text_links, $subdomain;
    if($loop_key !== false) $placeholder .= ':'.$loop_key;
    if(!$text_links) return $description;
    if($placeholder != 'T' AND empty($text_links[$placeholder])) $placeholder = 'T';
    if(empty($text_links[$placeholder])) return $description;
    $link = array_pop($text_links[$placeholder]);
    if($link['link_type'] == 'I')
        $link['anchor'] = '<img src="/images/'.$subdomain['subdomain_id'].'/'.seo_name($link['anchor']).'.jpg" alt="'.$link['anchor'].'">';
    foreach($link['anchor_wrap'] as $tag)
        $link['anchor'] = '<'.$tag.'>'.$link['anchor'].'</'.$tag.'>';
    $link = (empty($link['link_params']['prefix']) ? '' : $link['link_params']['prefix'].' ').'<a href="'.$link['href'].'"'.(empty($link['link_params']['attr']) ? '' : ' '.$link['link_params']['attr']).'>'.$link['anchor'].'</a>'.(empty($link['link_params']['suffix']) ? '' : ' '.$link['link_params']['suffix']);
    $desc = explode(' ', $description);
    $rand = array_rand($desc);
    if($rand == 0) $rand = array_rand($desc);
    $desc = array_merge(array_slice($desc, 0, $rand), array($link), array_slice($desc, $rand));
    return implode(' ', $desc);
}
function parse_var($content, $echo = true, $in_function = false) {
    $content = replace_template_var($content, $in_function);
    if(strpos($content, ' ') !== false) {
        $_content = explode(' ', $content);
        if($f = array_pop($_content) AND function_exists($f)) {//todo: while?
            return ' echo '.$f.'('.implode(' ', $_content).')';
        }
    }
    //debug($content);
    return (($echo AND strpos($content, '=') === false) ? 'echo ' : '').$content;
}
function replace_template_var($content, $in_function = false) {
    global $page;
    return preg_replace_callback('#\$(\w+)(?:\.(\w+))?(?:\.(\w+))?#', function($n) use($page, $in_function) {
        $var = ($in_function ? '' : '@').'$'.$n[1];
        if(!$in_function AND isset($page[$n[1]]))
            $var = '@$page[\''.$n[1].'\']';
        if(isset($n[2]))
            $var .= '[\''.$n[2].'\']';
        if(isset($n[3]))
            $var .= '[\''.$n[3].'\']';
        return $var;
    }, $content);
}
function apply_holders($var) {
    global $placeholders;
    if($placeholders) 
        $var = strtr($var, $placeholders);
    $var = preg_replace('#\[.+?\]#', '', $var);
    return $var;
}
function holderize($arr) {
    if(!is_array($arr)) return $arr;
    $holders = array();
    foreach($arr as $k => $v) {
        if(is_array($v))
            foreach($v as $_k => $_v) {
                if(is_array($_v)) {
                    foreach($_v as $__k => $__v) {
                        if(is_array($__v)) continue;
                        $holders['['.$k.'.'.$_k.'.'.$__k.']'] = ucfirst($__v);
                    }
                } else 
                    $holders['['.$k.'.'.$_k.']'] = ucfirst($_v);
            }
        else
            $holders['['.$k.']'] = ucfirst($v);
    }
    return $holders;
}
function debug($var, $skip_entities = false) {
    if(!SEONID) return;
    if($skip_entities)
        echo '<pre>'.print_r($var, true).'</pre>';
    else
        echo '<pre>'.htmlentities(print_r($var, true)).'</pre>';
}
function redirect($url, $code = 302) {
    if(headers_sent()) {
        echo '<meta http-equiv="refresh" content="5; url='.$url.'">';
        debug('Redirecting to <a href="'.$url.'">'.$url.'</a>', true);
    } else {
        http_response_code($code);
        if($code == 301)
            header('HTTP/1.1 301 Moved Permanently');
        header('Location: '.$url, $code);
    }
    exit;
}
function not_found() {
    http_response_code(404);
    echo @file_get_contents('error/404.html');exit;
}