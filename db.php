<?php
if(!defined('DB_HOST')) {
//    define('DB_HOST', '10.135.14.148');//private ips are working only on the same region
    define('DB_HOST', '46.101.196.51');
    define('DB_NAME', 'blinkseo');
    define('INDIAN', true);
}

function db_retry() {
    global $mysqli, $mysqli_db_host, $last_query;
    debug($mysqli[$mysqli_db_host]->error.'|'.$last_query);
    if(defined('INDEXER') OR preg_match('#already exists|Unknown thread id#'/*table creation|kill process*/, $mysqli[$mysqli_db_host]->error)) return false;
    //todo: 'is marked as crashed and should be repaired' 'try to repair it'
    if(preg_match('#is marked as crashed|Unknown column|You have an error in your SQL syntax|Duplicate entry|doesn\'t exist#', $mysqli[$mysqli_db_host]->error)) {
        if(defined('KEEP_ALIVE')) return false;
        exit;
    }
    if(!$mysqli[$mysqli_db_host]->ping()) 
        db_connect(); 
    else 
        sleep(5);
    return true;
}
function db_connect() {
    global $mysqli, $mysqli_db_host, $mysqli_db_name;
    if(!$mysqli_db_host) $mysqli_db_host = DB_HOST;
    if(!$mysqli_db_name) $mysqli_db_name = DB_NAME;
    if(!isset($mysqli)) {
        $mysqli = array();
        register_shutdown_function(function() {
            global $mysqli;
            foreach($mysqli as $v)
                $v->close();
        });
    }
    if(isset($mysqli[$mysqli_db_host]) AND $mysqli[$mysqli_db_host]->ping()) return;
    if(defined('INDEXER') AND $mysqli_db_host == DB_HOST) {
        $db_user = 'indexerusr';
        $db_pass = 'B&Bz6Dg*l#Y0';
    } elseif(defined('INDIAN') OR $mysqli_db_host != DB_HOST) {
        $db_user = 'indian';
        $db_pass = '#jYP#WYZ*c1i';
    } else {
        $db_user = DB_USER;
        $db_pass = DB_PASS;
    }
    $mysqli[$mysqli_db_host] = mysqli_init();
    $mysqli[$mysqli_db_host]->options(MYSQLI_OPT_CONNECT_TIMEOUT, defined('INDEXER') ? 2 : 60);
    while(!@$mysqli[$mysqli_db_host]->real_connect($mysqli_db_host, $db_user, $db_pass, $mysqli_db_name)) {
        if(!defined('INDIAN') AND !defined('KEEP_ALIVE')) debug('cannot connect to '.$mysqli_db_host.'. retrying...');
        if(defined('INDEXER')) return false;
        sleep(5);
    }
    //comment because default
    //if(!$mysqli[$mysqli_db_host]->multi_query('SET NAMES utf8 COLLATE utf8_general_ci;SET CHARACTER SET utf8')) return false;
    //while($mysqli[$mysqli_db_host]->more_results() AND $mysqli[$mysqli_db_host]->next_result()) {}
    return true;
}
function db_select_db($db_name = DB_NAME, $db_host = DB_HOST) {
    global $mysqli, $mysqli_db_host, $mysqli_db_name;
    if($db_name == $mysqli_db_name AND $db_host == $mysqli_db_host) return;
    $mysqli_db_name = $db_name;
    $mysqli_db_host = $db_host;
    if(!isset($mysqli) OR !isset($mysqli[$mysqli_db_host])) db_connect();
    while(!(@$mysqli[$mysqli_db_host]->select_db($db_name))) {if(!db_retry()) return false;}
    //comment because default
    //if(!$mysqli[$mysqli_db_host]->multi_query('SET NAMES utf8 COLLATE utf8_general_ci;SET CHARACTER SET utf8')) return false;
    //while($mysqli[$mysqli_db_host]->more_results() AND $mysqli[$mysqli_db_host]->next_result()) {}
}
function db_get_array($query) {
    global $mysqli, $mysqli_db_host;
    if(!isset($mysqli) OR !isset($mysqli[$mysqli_db_host])) db_connect();
    $args = func_get_args();
    if(array_key_exists(1, $args)) $query = db_process($query, array_slice($args, 1));
    while(!($result = @$mysqli[$mysqli_db_host]->query($query, MYSQLI_USE_RESULT))) {if(!db_retry()) return false;}
    $fields = $result->fetch_all(MYSQLI_ASSOC);
    $result->free();
    return $fields;
}
function db_get_hash_array($query, $field) {
    global $mysqli, $mysqli_db_host;
    if(!isset($mysqli) OR !isset($mysqli[$mysqli_db_host])) db_connect();
	$args = func_get_args();
    if(array_key_exists(2, $args)) $query = db_process($query, array_slice($args, 2));
    while(!($result = @$mysqli[$mysqli_db_host]->query($query, MYSQLI_USE_RESULT))) {if(!db_retry()) return false;}
    $fields = $result->fetch_all(MYSQLI_ASSOC);
    $result->free();
    $result = array();
    foreach($fields as $v)
        if(isset($v[$field]))
            $result[$v[$field]] = $v;
    return $result;
}
function db_get_row($query) {
    global $mysqli, $mysqli_db_host;
    if(!isset($mysqli) OR !isset($mysqli[$mysqli_db_host])) db_connect();
    $args = func_get_args();
    if(array_key_exists(1, $args)) $query = db_process($query, array_slice($args, 1));
    while(!($result = @$mysqli[$mysqli_db_host]->query($query, MYSQLI_USE_RESULT))) {if(!db_retry()) return false;}
    $fields = $result->fetch_assoc();
    $result->free();
    return $fields;
}
function db_get_field($query) {
    global $mysqli, $mysqli_db_host;
    if(!isset($mysqli) OR !isset($mysqli[$mysqli_db_host])) db_connect();
    $args = func_get_args();
    if(array_key_exists(1, $args)) $query = db_process($query, array_slice($args, 1));
    while(!($result = @$mysqli[$mysqli_db_host]->query($query, MYSQLI_USE_RESULT))) {if(!db_retry()) return false;}
    $field = $result->fetch_row();
    $result->free();
    return $field[0];
}
function db_get_fields($query) {
    global $mysqli, $mysqli_db_host;
    if(!isset($mysqli) OR !isset($mysqli[$mysqli_db_host])) db_connect();
    $args = func_get_args();
    if(array_key_exists(1, $args)) $query = db_process($query, array_slice($args, 1));
    while(!($result = @$mysqli[$mysqli_db_host]->query($query, MYSQLI_USE_RESULT))) {if(!db_retry()) return false;}
    $fields = $result->fetch_all();
    $result->free();
    foreach($fields as $k => $field)
        $fields[$k] = $field[0];
    return $fields;
}
function db_get_hash_multi_array($query, $params) {
    global $mysqli, $mysqli_db_host;
    if(!isset($mysqli) OR !isset($mysqli[$mysqli_db_host])) db_connect();
    @list($field, $field_2, $value) = $params;
    $args = func_get_args();
    if(array_key_exists(2, $args)) $query = db_process($query, array_slice($args, 2));
    while(!($result = @$mysqli[$mysqli_db_host]->query($query, MYSQLI_USE_RESULT))) {if(!db_retry()) return false;}
    $fields = $result->fetch_all(MYSQLI_ASSOC);
    $result->free();
    $result = array();
    foreach($fields as $v)
        if (!empty($field_2))
            $result[$v[$field]][$v[$field_2]] = !empty($value) ? $v[$value] : $v;
        else
            $result[$v[$field]][] = !empty($value) ? $v[$value] : $v;
    return $result;
}
function db_get_hash_single_array($query, $params) {
    global $mysqli, $mysqli_db_host;
    if(!isset($mysqli) OR !isset($mysqli[$mysqli_db_host])) db_connect();
    @list($key, $value) = $params;
    $args = func_get_args();
    if(array_key_exists(2, $args)) $query = db_process($query, array_slice($args, 2));
    while(!($result = @$mysqli[$mysqli_db_host]->query($query, MYSQLI_USE_RESULT))) {if(!db_retry()) return false;}
    $fields = $result->fetch_all(MYSQLI_ASSOC);
    $result->free();
    $result = array();
    foreach($fields as $v)
        $result[$v[$key]] = $v[$value];
    return $result;
}
function db_query($query) {
    global $mysqli, $mysqli_db_host;
    if(!isset($mysqli) OR !isset($mysqli[$mysqli_db_host])) db_connect();
    $args = func_get_args();
    if(array_key_exists(1, $args)) $query = db_process($query, array_slice($args, 1));
    while(!($result = @$mysqli[$mysqli_db_host]->query($query))) {if(!db_retry()) return false;};
    return $mysqli[$mysqli_db_host]->insert_id ? $mysqli[$mysqli_db_host]->insert_id : $mysqli[$mysqli_db_host]->affected_rows;
}
function db_quote() {
	$args = func_get_args();
	$pattern = array_shift($args);
	return db_process($pattern, $args, false);
}
function db_str_replace($needle, $replacement, $subject, &$offset) {
	$pos = strpos($subject, $needle, $offset);
	$offset = $pos + strlen($replacement);
	return substr_replace($subject, $replacement, $pos, 2);
}
function db_intval($int) {
	return $int + 0;
}
function get_table_fields($table_name) {
    static $table_fields;
    if(!$table_fields) $table_fields = array();
    if(isset($table_fields[$table_name]))
        return $table_fields[$table_name];
	$structure = db_get_array('SHOW COLUMNS FROM '.$table_name);
	if (is_array($structure)) {
		$table_fields[$table_name] = array_column($structure, 'Field');
		return $table_fields[$table_name];
	}
	return false;
}
function check_table_fields($data, $table_name) {
	$_fields = get_table_fields($table_name);
	if (is_array($_fields)) {
		foreach ($data as $k => $v) {
			if (!in_array($k, $_fields)) {
				unset($data[$k]);
			}
		}
		if (func_num_args() > 2) {
			for ($i = 2; $i < func_num_args(); $i++) {
				unset($data[func_get_arg($i)]);
			}
		}
		return $data;
	}
	return false;
}
function db_field($field) {
	if (preg_match('/([\w]+)/', $field, $m) && $m[0] == $field)
		return $field;
	return '';
}
function db_process($pattern, $data = array()) {
    global $last_query;
	$command = 'get';
	// Check if query updates data in the database
	if (preg_match("/^(UPDATE|INSERT(?: IGNORE)? INTO|REPLACE INTO|DELETE FROM) (`?\w+`?) /", $pattern, $m)) {
		$table_name = $m[2];
		$command = ($m[1] == 'DELETE FROM') ? 'delete' : 'set';
	}
	if (!empty($data) && preg_match_all("/\?(i|s|l|d|a|n|u|e|p|w|f)+/", $pattern, $m)) {
		$offset = 0;
		foreach ($m[0] as $k => $ph) {
			if ($ph == '?u' || $ph == '?e') {
				$data[$k] = check_table_fields($data[$k], $table_name);

				if (empty($data[$k])) {
					return false;
				}
			}

			if ($ph == '?i') { // integer
				$pattern = db_str_replace($ph, db_intval($data[$k]), $pattern, $offset); // Trick to convert int's and longint's

			} elseif ($ph == '?s') { // string
				$pattern = db_str_replace($ph, "'" . addslashes($data[$k]) . "'", $pattern, $offset);

			} elseif ($ph == '?l') { // string for LIKE operator
				$pattern = db_str_replace($ph, "'%" . addslashes(str_replace("\\", "\\\\", $data[$k])) . "%'", $pattern, $offset);

			} elseif ($ph == '?d') { // float
				$pattern = db_str_replace($ph, sprintf('%01.2f', $data[$k]), $pattern, $offset);

			} elseif ($ph == '?a') { // array FIXME: add trim
				$data[$k] = !is_array($data[$k]) ? array($data[$k]) : $data[$k];
				$pattern = db_str_replace($ph, "'" . implode("', '", array_map('addslashes', $data[$k])) . "'", $pattern, $offset);

			} elseif ($ph == '?n') { // array of integer FIXME: add trim
				$data[$k] = !is_array($data[$k]) ? array($data[$k]) : $data[$k];
				$pattern = db_str_replace($ph, !empty($data[$k]) ? implode(', ', array_map('db_intval', $data[$k])) : "''", $pattern, $offset);

			} elseif ($ph == '?u' || $ph == '?w') { // update/condition with and
				$q = '';
				$clue = ($ph == '?u') ? ', ' : ' AND ';
				foreach($data[$k] as $field => $value) {
					$q .= ($q ? $clue : '') . '`' . db_field($field) . "` = '" . addslashes($value) . "'";
				}
				$pattern = db_str_replace($ph, $q, $pattern, $offset);

			} elseif ($ph == '?e') { // insert
				$pattern = db_str_replace($ph, '(`' . implode('`, `', array_map('addslashes', array_keys($data[$k]))) . "`) VALUES ('" . implode("', '", array_map('addslashes', array_values($data[$k]))) . "')", $pattern, $offset);

			} elseif ($ph == '?f') { // field/table/database name
				$pattern = db_str_replace($ph, db_field($data[$k]), $pattern, $offset);

			} elseif ($ph == '?p') { // prepared statement
				$pattern = db_str_replace($ph, $data[$k], $pattern, $offset);
			}
		}
	}
	$last_query = $pattern;
	return $pattern;
}
